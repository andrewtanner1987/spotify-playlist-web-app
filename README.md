# Spotify playlist web application
This app reads and writes playlist data from and to a given Spotify playlist. The app authenticates the host server against the Spotify API with credentials provided for a single user. All interactions with the playlist are authenticated against a single user so specific user logins are not required or supported.
Built on [Node.js](https://nodejs.org/) using [webpack](https://webpack.js.org/), [Vue.js](https://vuejs.org/) and [socket.io](https://socket.io/).

## Description
This project consists of a back-end written in Node.js while the front-end is handled by Vue.js. The application is designed to allow limited functionality for adding tracks to a Spotify playlist. The back-end caches the Spotify playlist while the front-end provides the UI for users to search the Spotify tracks library and add results to the playlist. socket.io provides a real-time communication layer between the back and front-end. The cache is updated when a user loads the app in a browser or adds a song to the playlist.

## Dependencies
* [Node.js](https://nodejs.org/) >= 8.0
* [Yarn](https://yarnpkg.com) >= 1.12.3
* [Docker](https://www.docker.com/) (optional). The Docker container uses a Node.js version 8 base image so there is no need to install Node.js and Yarn if using Docker.

### Optional Docker usage
Running `docker-compose up` in `/` will get you going. This fires up a simple Docker container using a Node.js version 8 image. Docker is only used as a development environment and is not used in production.

There is some basic config in `.example.env` to ensure ports for the Express server as well as `webpack-dev-server` are exposed to the host machine. Save these changes as a `.env` file in `/`, which is ignored by Git. There is a `DNS` setting to provide better DNS for Docker to resolve internally.

You can choose to run the app inside the Docker container with the Docker `exec` command. You need to know the container name which you can find by running `docker ps` once the container is `Attaching...` after running `docker-compose up`. Then type `docker exec -it <container name> <path to bash>`, e.g `docker exec -it weddingwebsite_node_1 bash`.

## App description
The app is largely split over `/server` and `/ui` with a few other directories between to keep things organised and available globally.

`/` contains `package.json` for back-end requirements where as `/ui` has it's packages localised for front-end only.

* `/cache` should contain only one file, `playlist.json`, that is created the first time the app is run. This is pulled and updated from Spotify whenever a user loads the application and can have an optional cache expiry set in `/config.js` by setting `playlist_cache_expire` the time in seconds. `/config.js` file should be duplicated from `/config.example.js`. 

* `/server` contains an Express server in `/public` where the web server should reverse proxy to when in production. `/auth` contains the logic for an OAuth2 authentication flow which is used for initial authentication with the Spotify API authorisation endpoint. `/auth` will contain `access` and `refresh` tokens once an authentication request is successful. These are ignored by Git.

* `/spotify` contains the majority of the functionality once the app is up and running. Spotify endpoints are configured here as well as functions to read and write to a playlist and for searching the Spotify library.

* `/ui` contains all the front-end for the application. webpack is used for managing bundles with an optimised build available as well as `webpack-dev-server` for ease when in development. Vue.js is used to react to state changes based on return values of Spotify API calls as well as the contents of `/cache/playlist.json`.

* `/utils` contains a few helper functions to get things done!

* `/app.js` is a simple script to kick things off. Any process managers, such as [PM2](http://pm2.keymetrics.io/), should use this as the init script.

## Instructions
Follow these steps to get up and running:

* (Optional) If you're using Docker, copy the contents of `/src/.example.env` to `/src/.env` and set accordingly, otherwise, skip to step 1. `DNS` (for a DNS server) , `PUBLIC_HOST_PORT` and `PUBLIC_PORT_CONTAINER` are required. `WATCH_HOST_PORT` and `WATCH_PORT_CONTAINER` are optional and should be set if you plan on using `webpack-dev-server`. This sets ports to be exposed in and out of Docker. Docker mounts `/src` to `/var/www` in the container.

1. Run `yarn install` in `/src/`.

2. Create `/src/config.js`. You can duplicate `/src/config.example.js` and change settings accordingly.

3. Create `./src/.env`. You can duplicate `/src/.example.env` and change settings accordingly.

4. Run `yarn install` in `/src/ui`.

5. Run `yarn build` in `/src/ui`. This creates a production build of the UI in `/src/public`.

6. At this point start up an Express server listening on the `public_port` provided in `/src/config.js`. Just run `yarn app` in `/src` after ensuring that the following credentials are also present in `/src/.env`  :

    * `CLIENT_ID` from the Spotify [app settings](https://developer.spotify.com/dashboard/)
    * `CLIENT_SECRET` from the Spotify [app settings](https://developer.spotify.com/dashboard/)
    * `PLAYLIST_ID` of your Spotify playlist of choice. This is the playlist that will be read from and written to.

    Also ensure that the app callback URL is whitelisted on the Spotify [app settings](https://developer.spotify.com/dashboard/). This should be equal to protocol and hostname followed by `/auth/callback`, e.g `http://192.168.99.100/auth/callback`/. Don't forget to add both development and production URLs.

7. Finally, visit the app in a browser, e.g `http://192.168.99.100/`. If the app has not been previously authenticated, a simple OAuth2 authentication flow is established, access and refresh tokens saved in `/src/server/auth`. Then the browser is redirected to `/`. You may need to refresh the page as the first visit will generate the cache playlist file only after authentication has happened when the app is run in the browser for the first time.

## Problems with Spotify API authentication
If reauthentication is required, just delete `access.json` and `refresh.json` in `/src/server/auth`.

## Development build of UI
Run `yarn dev` or `yarn watch` in `/src/ui`. If running `yarn watch` ensure that ports are configured for `webpack-dev-server` in `.env` (`WATCH_PORT_HOST` and `WATCH_PORT_CONTAINER`) (if using Docker) and `/src/config.js` (`watch_port`). Also, be sure to keep the Express server alive to serve up static assets with `yarn app`, even when using `yarn watch`.

## iFrame usage
If using the app in an iFrame it may be necessary to allow certain requesting origins. This can be done with the `allow_origin` setting in `/src/config.js`.

## Browser compatibility
Efforts have been made to ensure browser compatibility including latest Chrome (Android, Mac OS and Windows), Firefox (Mac OS and Windows), Safari (Mac OS and iOS), Edge and Internet Explorer 11.