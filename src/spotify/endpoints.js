require('dotenv').config('../');

module.exports = {
    search: `https://api.spotify.com/v1/search`,
    playlists: `https://api.spotify.com/v1/playlists/${process.env.PLAYLIST_ID}/tracks`
}