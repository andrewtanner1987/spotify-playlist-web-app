const Utils = require('../utils/utils.js');
const Endpoints = require('./endpoints.js');

const fs = require('fs');
const AuthLib = require('../server/auth/auth.js');
const Auth = require('simple-oauth2').create(AuthLib.credentials);
const Fetch = require('node-fetch');

const Config = require('../config.js'); 

module.exports = {
    playlists: {
        get: {
            tempItems: [],
            read(endpoint = Endpoints.playlists) {
                AuthLib.refreshToken(Auth)
                .then(() => {
                    const Token = Utils.getAccessToken();
    
                    Fetch(endpoint, {
                        headers: {
                            "Authorization": `Bearer ${Token.access_token}`
                        }
                    })
                    .then(result => result.json())
                    .then(json => {
                        if (json.next) {
                            this.store(json.items);
                            this.read(json.next);
                        } else {
                            this.store(json.items);
                            this.write(json);
                        }
                    });
                });
            },
            store(data) {
                this.tempItems = this.tempItems.concat(data);
            },
            write(json) {
                json.items = this.tempItems;
                this.tempItems = [];
                fs.writeFileSync('./cache/playlist.json', JSON.stringify(json));
            },
            init() {
                this.read();
            }
        },
        post(track) {
            AuthLib.refreshToken(Auth)
            .then(() => {
                const Token = Utils.getAccessToken();

                Fetch(Endpoints.playlists, {
                    headers: {
                        "Authorization": `Bearer ${Token.access_token}`,
                        "Content-Type": "application/json"
                    },
                    method: "post",
                    body: JSON.stringify({
                        uris: [`spotify:track:${track}`]
                    })
                })
                .then(() => {
                    this.get.read();
                });
            });
        }
    },
    search: {
        search(socketID, endpoint, paging) {
            AuthLib.refreshToken(Auth)
            .then(() => {
                const Token = Utils.getAccessToken();
                const io = require('socket.io-client');
                const socketClient = io.connect(Config.host);
    
                socketClient.on('connect', () => {
                    Fetch(endpoint, {
                        headers: {
                            "Authorization": `Bearer ${Token.access_token}`,
                        }
                    })
                    .then(result => result.json())
                    .then(json => {
                        socketClient.emit('searchResults', {
                            json: json,
                            socketID: socketID,
                            paging: paging
                        });
                    });
                });
            });
        },
        new(data) {
            this.search(data.socketID, `${Endpoints.search}?q="${encodeURIComponent(data.query)}"&type=track&market=GB`, false);
        },
        page(data) {
            this.search(data.socketID, data.endpoint, true);
        }
    }
}