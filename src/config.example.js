module.exports = {
    host: 'http://192.168.99.100:8080',
    public_port: 8080,
    watch_port: 8081,
    playlist_cache_expire: 0,
    allow_origin: '*'
}