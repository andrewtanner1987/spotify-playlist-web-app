const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const AuthLib = require('../auth/auth');
const OAuth2 = require('simple-oauth2').create(AuthLib.credentials);

const Config = require('../../config.js'); 
const Spotify = require('../../spotify/api.js');
const Utils = require('../../utils/utils.js');

module.exports = {
    init() {
        app.get('/', (req, res) => {
            if (!AuthLib.haveToken()) {
                res.redirect('/auth');
            } else {
                if (Utils.cacheHasExpired()) {
                    Spotify.playlists.get.init();
                }
    
                res.sendFile('index.html', {
                    root: __dirname + '../../../public/'
                }, err => {
                    if (err) console.log(err);
                });
    
                app.use('/', express.static(__dirname + '../../../public/'));
            }
        });

        app.get('/auth', (req, res) => {
            if (!AuthLib.haveToken()) {
                res.redirect('/auth/request-code');
            } else {
                res.redirect('/');
            }
        });

        app.get('/auth/request-code', (req, res) => {
            if (!AuthLib.haveToken()) {            
                res.redirect(AuthLib.requestCode(OAuth2));
            } else {
                res.redirect('/');
            }
        });

        app.get('/auth/callback', (req, res) => {
            if (!AuthLib.haveToken()) {
                AuthLib.requestToken(OAuth2, req.query.code)
                .then(() => {
                    res.redirect('/');
                });
            } else {
                res.redirect('/');
            }
        });

        app.get('/auth/refresh-token', (req, res) => {
            AuthLib.refreshToken(OAuth2)
            .then(() => {
                res.redirect('/');
            });
        });

        app.get('/cache/playlist.json', (req, res) => {
            res.sendFile('playlist.json', {
                root: __dirname + '../../../cache',
                headers: {
                    'Access-Control-Allow-Origin': Config.allow_origin
                }
            }, err => {
                if (err) console.log(err);
            });
        });

        io.on('connection', socket => {
            console.log(`Browser connected to socket.io on ${Config.public_port}...`);
            
            socket.on('addToPlaylist', data => {
                Spotify.playlists.post(data.id);
                io.emit('newTrack', data);
            });
        
            socket.on('search', data => {
                Spotify.search.new(data);
            });
        
            socket.on('page', data => {
                Spotify.search.page(data);
            });
        
            socket.on('searchResults', data => {
                io.to(data.socketID).emit('searchResults', data);
            });
        });
        
        server.listen(Config.public_port, () => {
            console.log(`Express server listening on ${Config.public_port}...`);
        });
    }
}