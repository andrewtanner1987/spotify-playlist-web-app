require('dotenv').config('../../');

const fs = require('fs');

const Config = require('../../config');
const AuthConfig = {
    redirect_uri: `${Config.host}/auth/callback`,
    scope: 'playlist-modify-private'
};

module.exports = {
    credentials: {
        client: {
            id: process.env.CLIENT_ID,
            secret: process.env.CLIENT_SECRET
        },
        auth: {
            authorizeHost: 'https://accounts.spotify.com',
            authorizePath: '/authorize',
            tokenHost: 'https://accounts.spotify.com',
            tokenPath: '/api/token'
        }
    },
    requestCode(auth) {
        const authorisationUri = auth.authorizationCode.authorizeURL({
            redirect_uri: AuthConfig.redirect_uri,
            scope: AuthConfig.scope
        });

        return authorisationUri;
    },
    async requestToken(auth, code) {
        const tokenConfig = {
            code: code,
            redirect_uri: AuthConfig.redirect_uri,
            scope: AuthConfig.scope
        };

        try {
            const result = await auth.authorizationCode.getToken(tokenConfig);
            const accessToken = auth.accessToken.create(result);

            fs.writeFileSync('./server/auth/refresh.json', JSON.stringify(accessToken.token.refresh_token));
            fs.writeFileSync('./server/auth/access.json', JSON.stringify(accessToken.token));
        } catch (err) {
            console.log('Access Token error: ', err.message);
        }
    },
    async refreshToken(auth) {
        const Refresh = JSON.parse(fs.readFileSync('./server/auth/refresh.json', 'utf8'));
        const Access = JSON.parse(fs.readFileSync('./server/auth/access.json', 'utf8'));
        const TokenObj = {
            access_token: Access.access_token,
            refresh_token: Refresh,
            expires_at: Access.expires_at
        };

        let accessToken = auth.accessToken.create(TokenObj);

        if (accessToken.expired()) {
            try {
                await Promise.all([
                    accessToken = await accessToken.refresh(),
                    fs.writeFileSync('./server/auth/access.json', JSON.stringify(accessToken.token))
                ]);
            } catch (err) {
                console.log('Refresh Token error: ', err);
            }
        }
    },
    haveToken() {
        return fs.existsSync('./server/auth/access.json');
    }
}