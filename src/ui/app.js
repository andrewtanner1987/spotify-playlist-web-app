import '@babel/polyfill';
import 'whatwg-fetch';
import 'intersection-observer';

import Config from '../config.js';

import io from 'socket.io-client';

import Vue from 'vue';
import VueSocketIO from 'vue-socket.io-extended';
import App from './components/app.vue'

require('./css/app.scss');

const socket = io(Config.host);

Vue.use(VueSocketIO, socket);

new Vue({
    render: h => h(App)
}).$mount('#app');