require("dotenv").config("../");

const WebpackMerge = require('webpack-merge');

const Base = require('./webpack.base.config');

const Config = require('../config');

module.exports = WebpackMerge.smart(
    Base,
    {
        mode: "development",
        devServer: {
            host: "0.0.0.0",
            hot: true,
            port: Config.watch_port,
            watchOptions: {
                poll: true
            }
        }
    }
);