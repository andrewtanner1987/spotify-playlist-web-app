const HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
    entry: {
        app: "./app.js"
    },
    devtool: "cheap-eval-source-map",
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
                exclude: "/node_modules/"
            },
            {
                test: /\.woff2?$/,
                loader: "file-loader"
            },
            {
                test: /\.s?css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                loader: "babel-loader?cacheDirectory"
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "./html/index.html"
        })
    ],
    optimization: {
        splitChunks: {
            chunks: "all"
        }
    }
}
