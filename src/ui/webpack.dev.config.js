const Path = require("path");

const WebpackMerge = require("webpack-merge");
const Base = require('./webpack.base.config');
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = WebpackMerge.smart(
    Base,
    {
        mode: "development",
        devtool: "cheap-eval-source-map",
        plugins: [
            new CleanWebpackPlugin(["public"], {
                root: __dirname.replace("ui", "")
            }),
        ],
        output: {
            path: Path.resolve(__dirname, '../public')
        }
    }
);