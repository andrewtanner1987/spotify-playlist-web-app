const fs = require('fs');
const Config = require('../config.js');

module.exports = {
    fileLastUpdated(thePath) {
        if (!fs.existsSync(thePath)) {
            return 0;
        } else {
            return parseInt(Math.round(fs.statSync(thePath).mtimeMs));
        }
    },
    cacheHasExpired() {
        return (Date.now() - this.fileLastUpdated('/cache/playlist.json')) >= Config.playlist_cache_expire * 1000;
    },
    getAccessToken() {
        return JSON.parse(fs.readFileSync('./server/auth/access.json', 'utf8'));
    }
}